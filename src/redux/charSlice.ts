import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import Character from "api/models/Character";
import Movie from "api/models/Movie";
import { fetchMoviesByCharacter, fetchCharacters } from "redux/asyncRequest";

interface CharState {
  searchResult: Character[];
  selected: Character | null;
  movies: Movie[];
  error: string | null;
}

export const initialState = {
  selected: null,
  movies: [],
  searchResult: [],
  error: null,
} as CharState;

export const charSlice = createSlice({
  name: "char",
  initialState: initialState,
  reducers: {
    setMovies: (state, action: PayloadAction<Movie[]>) => {
      state.movies = action.payload;
    },
    setSearchResult: (state, action: PayloadAction<Character[]>) => {
      state.searchResult = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCharacters.fulfilled, (state, action) => {
      state.error = null;
      state.searchResult = action.payload;
    });
    builder.addCase(fetchCharacters.rejected, (state, action) => {
      state.error = "Error during fetching characters";
    });
    builder.addCase(fetchMoviesByCharacter.fulfilled, (state, action) => {
      state.error = null;
      state.movies = action.payload.movies;
      state.selected = action.payload.character;
    });
    builder.addCase(fetchMoviesByCharacter.rejected, (state, action) => {
      state.error = "Error during fetching movies";
    });
  },
});

export default charSlice.reducer;
