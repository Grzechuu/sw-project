import { configureStore, combineReducers } from "@reduxjs/toolkit";

import charReducer from "redux/charSlice";

const rootReducer = combineReducers({
  char: charReducer,
});

export const store = configureStore({
  reducer: rootReducer,
});

export type AppState = ReturnType<typeof rootReducer>;

export type AppDispatch = typeof store.dispatch;
