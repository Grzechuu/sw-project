import { createAsyncThunk } from "@reduxjs/toolkit";
import api, { fetchAllPlanets, fetchMultiple } from "api/api";
import Character from "api/models/Character";
import Movie from "api/models/Movie";

export const fetchCharacters = createAsyncThunk(
  "character/fetchBySearch",
  async (searchQuery: string) => {
    const peopleByName = await api.searchPeople(searchQuery);
    const planetsByName = await api.searchPlanets(searchQuery);
    const allPlanets = await fetchAllPlanets();
    const planetsWithPopulation = allPlanets.filter(
      (planet) => planet.population === searchQuery
    );
    const charactersResultsFromPlantsSearch = [
      ...planetsByName.data.results.reduce((acc, curr) => {
        curr.residents.forEach((item) => acc.push(item));
        return acc;
      }, [] as string[]),
      ...planetsWithPopulation.reduce((acc, curr) => {
        curr.residents.forEach((item) => acc.push(item));
        return acc;
      }, [] as string[]),
    ];

    const characterFromPlanetsSet = Array.from(
      new Set(charactersResultsFromPlantsSearch)
    );
    const charactersFromPlanets = await fetchMultiple<Character>(
      characterFromPlanetsSet
    );

    return [...peopleByName.data.results, ...charactersFromPlanets];
  }
);

export const fetchMoviesByCharacter = createAsyncThunk(
  "character/movies",
  async (character: Character) => {
    const movies = await fetchMultiple<Movie>(character.films);
    return {
      movies,
      character,
    };
  }
);

export const fetchMoviesByCharacterId = createAsyncThunk(
  "characterId/movies",
  async (characterId: string, thunkAPI) => {
    const response = await api.fetchCharacterById(characterId);
    return await thunkAPI.dispatch(fetchMoviesByCharacter(response.data));
  }
);
