const mockedApiResponse = {
  data: {
    results: [],
  },
};
export const toMockedData = (data: any) => {
  return {
    data: {
      results: data,
    },
  };
};
const api = {
  searchPeople: jest.fn().mockReturnValue(mockedApiResponse),
  searchPlanets: jest.fn().mockReturnValue(mockedApiResponse),
  fetchCharacterById: jest.fn().mockReturnValue(mockedApiResponse),
  fetchPlanets: jest.fn().mockReturnValue(mockedApiResponse),
  fetchByUrl: jest.fn().mockReturnValue(mockedApiResponse),
};

export const fetchMultiple = jest.fn().mockReturnValue([]);
export const fetchAllPlanets = jest.fn().mockReturnValue([]);

export default api;
