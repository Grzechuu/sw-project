import React, { ReactElement } from "react";
import {
  cleanup,
  fireEvent,
  render,
  RenderOptions,
  screen,
  waitFor,
} from "@testing-library/react";
import App from "App";
import { Router } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "redux/store";
import history from "utils/history";
import api, {
  fetchAllPlanets,
  fetchMultiple,
  toMockedData,
} from "__mocks__/api/api";
import userEvent from "@testing-library/user-event";

const renderWithRouter = (
  ui: ReactElement,
  route = "/",
  renderOptions?: RenderOptions
) => {
  history.push(route);
  const wrapper: React.FC = ({ children }) => {
    return (
      <Provider store={store}>
        <Router history={history}>{children}</Router>
      </Provider>
    );
  };
  return render(ui, { wrapper, ...renderOptions });
};
describe("main component behavior", () => {
  const inputPlaceholder = "full name | home world | population";
  beforeEach(() => {
    jest.clearAllMocks();
  });
  afterEach(cleanup);
  test("home page renders initial components", () => {
    renderWithRouter(<App />);
    expect(screen.getByPlaceholderText(inputPlaceholder)).toBeInTheDocument();
    expect(
      screen.getByText(
        "Search character by full name | home world | home world population"
      )
    ).toBeInTheDocument();
    expect(screen.getByRole("button", { name: "Submit" })).toBeInTheDocument();
  });

  test("searchbox field has focus on label click", async () => {
    renderWithRouter(<App />);
    const input = screen.getByPlaceholderText(inputPlaceholder);
    const label = screen.getByText(
      "Search character by full name | home world | home world population"
    );
    userEvent.click(label);
    await waitFor(() => {
      expect(input).toHaveFocus();
    });
  });

  test("does not submit blank form", async () => {
    renderWithRouter(<App />);

    const handleSubmit = jest.fn();
    const button = screen.getByRole("button", { name: "Submit" });
    await waitFor(() => {
      fireEvent.click(button);
    });
    expect(handleSubmit).toHaveBeenCalledTimes(0);
  });

  test("show spinner while loading", async () => {
    renderWithRouter(<App />);

    const input = screen.getByPlaceholderText(inputPlaceholder);
    const button = screen.getByRole("button", { name: "Submit" });
    fireEvent.change(input, { target: { value: "tatooine" } });
    expect(button).not.toContainHTML("span");
    fireEvent.click(button);
    expect(screen.getByRole("button")).toContainHTML("span");
  });

  test("api call on form submit", async () => {
    renderWithRouter(<App />);
    const input = screen.getByPlaceholderText(inputPlaceholder);
    const button = screen.getByRole("button", { name: "Submit" });
    fireEvent.change(input, { target: { value: "tatooine" } });
    fireEvent.click(button);
    expect(api.searchPeople).toHaveBeenCalledTimes(1);
  });
  test("displays no items message", async () => {
    renderWithRouter(<App />, "/?search=tatooine");
    await waitFor(() => {
      expect(screen.getByText("No items")).toBeInTheDocument();
    });
  });
  test("displays error", async () => {
    api.searchPeople.mockReset();
    renderWithRouter(<App />, "/?search=tatooine");
    await waitFor(() => {
      expect(
        screen.getByText("Error during fetching characters")
      ).toBeInTheDocument();
    });
  });
  test("displays correct items count on result list", async () => {
    renderWithRouter(<App />);
    api.searchPeople.mockReturnValueOnce(
      toMockedData([
        {
          name: "character1",
          homeworld: "",
          films: [""],
          url: "1",
        },
        {
          name: "character2",
          homeworld: "",
          films: [""],
          url: "2",
        },
        {
          name: "character3",
          homeworld: "",
          films: [""],
          url: "3",
        },
      ])
    );
    const input = screen.getByPlaceholderText(inputPlaceholder);
    const button = screen.getByRole("button", { name: "Submit" });
    fireEvent.change(input, { target: { value: "tatooine" } });
    fireEvent.click(button);

    fireEvent.focus(input);
    const list = screen.getByTestId("searchList");
    expect(list).toContainHTML("div");
    expect(list.firstChild).toHaveClass("search-results__loader");
    await waitFor(() => {
      expect(api.searchPlanets).toHaveBeenCalledTimes(1);
      expect(api.searchPeople).toHaveBeenCalledTimes(1);
      expect(fetchAllPlanets).toHaveBeenCalledTimes(1);
    });
    expect(list).toBeInTheDocument();
    expect(list).toHaveClass("search-results--show");
    expect(list.children.length).toEqual(3);
    expect(history.location.search).toEqual("?search=tatooine");
  });
  test("sets querystring correctly", async () => {
    renderWithRouter(<App />, "/?search=tatooine&characterId=1");

    await waitFor(() => {
      api.searchPeople.mockReturnValueOnce(
        toMockedData([
          {
            name: "character1",
            homeworld: "",
            films: [""],
            url: "1",
          },
          {
            name: "character2",
            homeworld: "",
            films: [""],
            url: "2",
          },
          {
            name: "character3",
            homeworld: "",
            films: [""],
            url: "3",
          },
        ])
      );
      api.fetchCharacterById.mockReturnValueOnce({
        data: {
          name: "test character",
          homeworld: "",
          films: ["film1"],
          url: "http://moocked/api/people/7/",
        },
      });
    });
    fetchMultiple.mockReturnValueOnce([
      {
        title: "movie1",
        episode_id: 1,
        opening_crawl: "lorem ipsum",
        release_date: "2021-01-01",
      },
      {
        title: "movie2",
        episode_id: 2,
        opening_crawl: "lorem ipsum",
        release_date: "2021-01-01",
      },
      {
        title: "movie3",
        episode_id: 3,
        opening_crawl: "lorem ipsum",
        release_date: "2021-01-01",
      },
    ]);
    const input = screen.getByDisplayValue("tatooine");
    expect(input).toBeInTheDocument();
    expect(screen.getByText("test character")).toBeInTheDocument();
  });
});
