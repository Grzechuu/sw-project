import React from "react";
import { Route, Switch } from "react-router-dom";
import { Router } from "react-router";
import "App.scss";
import MainPage from "MainPage";
import history from "utils/history";
import { Provider } from "react-redux";
import { store } from "redux/store";

const App: React.FC = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Switch>
          <Route exact path={"/"} component={MainPage} />
        </Switch>
      </Router>
    </Provider>
  );
};

export default App;
