import axios from "./axios";
import { AxiosResponse } from "axios";
import { PagedResponse } from "api/models/PagedResponse";
import Planet from "api/models/Planet";
import Character from "api/models/Character";

const api = {
  searchPeople: (
    searchQuery: string
  ): Promise<AxiosResponse<PagedResponse<Character[]>>> => {
    return axios.get(`/people/?search=${searchQuery}`);
  },
  searchPlanets: (
    searchQuery: string
  ): Promise<AxiosResponse<PagedResponse<Planet[]>>> => {
    return axios.get(`/planets/?search=${searchQuery}`);
  },
  fetchCharacterById: (
    characterId: string
  ): Promise<AxiosResponse<Character>> => {
    return axios.get(`/people/${characterId}`);
  },

  fetchPlanets: (
    page: number
  ): Promise<AxiosResponse<PagedResponse<Planet[]>>> => {
    return axios.get("/planets/", {
      data: null,
      params: {
        page,
      },
    });
  },
  fetchByUrl: <T>(url: string): Promise<AxiosResponse<T>> => {
    //WORKAROUND FOR HEROKU HOST LIMITATION
    return axios.get(url.replace("http", "https"));
  },
};

export const fetchAllPlanets = async (): Promise<Planet[]> => {
  let planets = [] as Planet[];
  try {
    let page = 0;
    let nextPage: boolean;
    do {
      page++;
      nextPage = false;
      const response = await api.fetchPlanets(page);
      planets = [...planets, ...response.data.results];
      if (response.data.next) {
        nextPage = true;
      }
    } while (nextPage);
    return planets;
  } catch (err) {
    return planets;
  }
};

export const fetchMultiple = async <T>(
  urlCollection: string[]
): Promise<T[]> => {
  return await urlCollection.reduce(async (previousAcc, curr) => {
    try {
      const acc = await previousAcc;
      const response = await api.fetchByUrl<T>(curr);
      acc.push(response.data);
      return acc;
    } catch (error) {
      return previousAcc;
    }
  }, Promise.resolve([] as T[]));
};

export default api;
