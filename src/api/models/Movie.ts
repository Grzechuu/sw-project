export default interface Movie {
  title: string;
  episode_id: number;
  opening_crawl: string;
  characters: string[];
  planets: string[];
  url: string;
  release_date: string;
}
