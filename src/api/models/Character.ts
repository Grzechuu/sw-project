export default interface Character {
  films: string[];
  homeworld: string;
  name: string;
  url: string;
}
