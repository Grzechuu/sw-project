export interface PagedResponse<T = never> {
  count: number;
  next: null | number;
  previous: null | number;
  results: T;
}
