import axios from "axios";
import * as dotenv from "dotenv";
dotenv.config();

const http = axios.create({
  baseURL: process.env.REACT_APP_API_URL,
});

export default http;
