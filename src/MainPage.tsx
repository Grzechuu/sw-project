import React, { useEffect, useRef, useState } from "react";
import Character from "api/models/Character";
import { useDispatch, useSelector } from "react-redux";
import { useLocation } from "react-router";
import { AppDispatch, AppState } from "redux/store";
import history from "utils/history";
import { ClipLoader } from "react-spinners";
import MovieList from "components/Movies";
import SearchList from "components/SearchList";
import {
  fetchMoviesByCharacter,
  fetchMoviesByCharacterId,
  fetchCharacters,
} from "redux/asyncRequest";

enum QueryParams {
  SEARCH = "search",
  CHARACTER_ID = "characterId",
}

const getCharacterIdFromURL = (character: Character) => {
  //TODO CHANGE SEARCH ID METHOD
  const r = /\d+/;
  const number = character.url.match(r);
  return number?.[0];
};

const MainPage: React.FC = () => {
  const [moviesLoading, setMoviesLoading] = useState(false);
  const [resultsLoading, setResultsLoading] = useState(false);

  const [isOpen, setIsOpen] = useState(false);
  const [searchValue, setSearchValue] = useState<string>("");
  const searchRef = useRef<HTMLDivElement | null>(null);
  const params = new URLSearchParams(useLocation().search);

  const [
    searchResults,
    movies,
    selected,
    error,
  ] = useSelector((state: AppState) => [
    state.char.searchResult,
    state.char.movies,
    state.char.selected,
    state.char.error,
  ]);

  const dispatch = useDispatch<AppDispatch>();

  const handleSearchParams = (key: string, value: string) => {
    params.set(key, value);
    history.push({
      search: params.toString(),
    });
  };

  const handleSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setSearchValue(e.target.value);
  };

  const handleCharactersFetch = async (searchValue: string) => {
    setResultsLoading(true);
    const response = await dispatch(fetchCharacters(searchValue));
    if (fetchCharacters.fulfilled.match(response)) {
      handleSearchParams(QueryParams.SEARCH, searchValue);
    }
    setResultsLoading(false);
  };

  const handleMoviesFetch = async (character: Character) => {
    setMoviesLoading(true);
    const response = await dispatch(fetchMoviesByCharacter(character));
    if (fetchMoviesByCharacter.fulfilled.match(response)) {
      const id = getCharacterIdFromURL(character);
      if (id) {
        handleSearchParams(QueryParams.CHARACTER_ID, id);
      }
    }
    setIsOpen(false);
    setMoviesLoading(false);
  };

  const handleCharacterMoviesFetch = async (characterId: string) => {
    setMoviesLoading(true);
    const response = await dispatch(fetchMoviesByCharacterId(characterId));
    if (fetchMoviesByCharacterId.fulfilled.match(response)) {
      handleSearchParams(QueryParams.CHARACTER_ID, characterId);
    }
    setMoviesLoading(false);
  };

  useEffect(() => {
    (async () => {
      const search = params.get(QueryParams.SEARCH);
      const characterId = params.get(QueryParams.CHARACTER_ID);
      if (search) {
        setSearchValue(search);
        await handleCharactersFetch(search);
      }
      if (search && characterId) {
        await handleCharacterMoviesFetch(characterId);
      }
    })();
  }, []);

  const toggleSearchBox = (event: MouseEvent) => {
    const t = event.target as Element;
    if (
      searchRef &&
      searchRef.current &&
      !searchRef.current.contains(event.target as Element) &&
      t.id !== "id-search"
    ) {
      setIsOpen(false);
    }
  };

  useEffect(() => {
    document.addEventListener("click", toggleSearchBox, true);
    return () => {
      document.removeEventListener("click", toggleSearchBox, true);
    };
  });

  const handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault();
    await handleCharactersFetch(searchValue);
    setIsOpen(true);
  };

  return (
    <div className="main-container">
      <label htmlFor="id-search" className="label truncate">
        Search character by full name | home world | home world population
      </label>
      <form onSubmit={handleSubmit} className="form">
        <div className="form__field">
          <div className="form__field--search">
            <input
              name="search"
              type="search"
              id="id-search"
              onFocus={() => {
                setIsOpen(true);
              }}
              className="custom-input"
              placeholder="full name | home world | population"
              onChange={handleSearch}
              value={searchValue}
              required
            />
            <SearchList
              ref={searchRef}
              isLoading={resultsLoading}
              searchResult={searchResults}
              handleItemClick={handleMoviesFetch}
              isOpen={isOpen}
            />
          </div>
          <button type="submit" className="custom-button">
            {resultsLoading ? (
              <ClipLoader color="#fff" size={10} data-testid={"spinner"} />
            ) : (
              "Submit"
            )}
          </button>
        </div>
      </form>
      <div className="title">Character movies</div>
      <div>
        <span className="character" data-testid="char-name">
          {selected ? selected.name : null}
        </span>
        <div className="movies-container" data-testid="movies-container">
          <MovieList movies={movies} isLoading={moviesLoading} />
        </div>
        {error && <span className="error">{error}</span>}
      </div>
    </div>
  );
};

export default MainPage;
