import React from "react";
import { ClipLoader } from "react-spinners";
import Character from "api/models/Character";

interface Props {
  isOpen: boolean;
  isLoading: boolean;
  searchResult: Character[];
  handleItemClick: (char: Character) => void;
}

const SearchList = React.forwardRef<HTMLDivElement, Props>(
  ({ isLoading, isOpen, searchResult, handleItemClick }, ref) => {
    const renderSearchResult = () => {
      if (isLoading) {
        return (
          <div className="search-results__loader">
            <ClipLoader color="#32325d" />
          </div>
        );
      } else if (searchResult.length > 0) {
        return searchResult.map((character) => (
          <div
            className="search-results__item"
            key={character.url}
            onClick={() => handleItemClick(character)}
          >
            {character.name}
          </div>
        ));
      } else {
        return <div className="search-results__item">No items</div>;
      }
    };

    return (
      <div
        data-testid={"searchList"}
        ref={ref}
        className={`${isOpen ? "search-results--show" : ""} search-results`}
      >
        {renderSearchResult()}
      </div>
    );
  }
);
export default SearchList;
