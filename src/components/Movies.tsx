import React from "react";
import Movie from "api/models/Movie";
import { ClipLoader } from "react-spinners";

interface Props {
  movies: Movie[];
  isLoading: boolean;
}
const MovieList: React.FC<Props> = ({ movies, isLoading }) => {
  return (
    <>
      {isLoading ? (
        <div className="movie__loader" data-testid="movie-loader">
          <ClipLoader color="#32325d" />
        </div>
      ) : (
        movies.map((movie) => {
          return (
            <div key={movie.episode_id} className="movie">
              <span className="movie__title">{movie.title}</span>
              <span className="movie__release-date">{movie.release_date}</span>
              <span className="movie__opening-crawl">
                {movie.opening_crawl.substr(0, 150) + "..."}
              </span>
            </div>
          );
        })
      )}
    </>
  );
};

export default MovieList;
